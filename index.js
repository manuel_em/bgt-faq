// init db models
require("./classes/MongooseModels");

var express = require("express"),
	conf = require("./conf"),
	routesFaq = require("./routes/faq");


var Module = {
	init : function (app, path, options) {
		if (path) conf.modulepath = path;
		app.use(conf.modulepath + "/css", express.static(__dirname + '/static/css'));
		app.get(conf.modulepath, routesFaq.index);
		app.get(conf.modulepath + "/search", routesFaq.search);
		app.get(conf.modulepath + "/create", routesFaq.create);
		app.post(conf.modulepath + "/create", routesFaq.createProcess);
		app.get(conf.modulepath + "/issue/:issue", routesFaq.issue);
		app.post(conf.modulepath + "/issue/:issue", routesFaq.issueProcess);

		if (options) {
			if (options.title) {
				conf.title = options.title;
			}
			if (options.viewspath) {
				conf.viewspath = options.viewspath;
			}
		}
	}
};

module.exports = Module;