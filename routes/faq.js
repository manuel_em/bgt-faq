var conf = require("../conf")
	FAQ = require("../classes/FAQ");

var routes = {};
routes.index = function (req, res) {
	res.render(conf.viewspath + 'create', {
		header : conf.getHeader()
	});
};
routes.search = function (req, res) {
	res.render(conf.viewspath + 'search', {
		header : conf.getHeader()
	});
};
routes.create = function (req, res) {
	res.render(conf.viewspath + 'create', {
		header : conf.getHeader()
	});
};
routes.createProcess = function (req, res) {
	var issuetxt = req.params.issue,
		tags = req.params.tags,
		i;
	if (!issuetxt) {
		res.render(conf.viewspath + 'create', {
			header : conf.getHeader(),
			form : req.body,
			error : "Vous devez renseigner une question."
		});
	}
	if (tags) {
		tags = tags.split(";");
		for (i = 0; i < tags.length; i++) {
			tags[i] = tags[i].replace(/^\s+/, "").replace(/\s+$/, "");
		}
	}
	FAQ.create({issue : issuetxt},function (err, issue) {

	});
	res.render(conf.viewspath + 'create', {
		header : conf.getHeader(),
		form : req.body
	});
};
routes.issue = function (req, res) {
	var issue = req.params.issue;
	res.render(conf.viewspath + 'create', {
		header : conf.getHeader()
	});
};
routes.issueProcess = function (req, res) {
	var params = req.body,
		issue = req.params.issue;
	if (params.comment) {

	} else if (params.answer) {

	} else {
		res.redirect(conf.modulepath + "/issue/" + issue);
	}
	res.render(conf.viewspath + 'create', {
		header : conf.getHeader()
	});
};
module.exports = routes;