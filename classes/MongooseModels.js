var FaqSchema = new mongoose.Schema({
	author : String,
	issue : String,
	answer : String,
	comments : {
		type : Array,
		default : []
	},
	tags : {
		type : Array,
		default : []
	},
	updatedate : {
		type: Date,
		default: Date.now
	},
	creationdate : {
		type: Date,
		default: Date.now
	}
});

var MongooseModel = {
	createInstance : function (dataname) {
		mongoose.model(dataname, FaqSchema);
	}
};
module.exports = MongooseModel;