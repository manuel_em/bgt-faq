var conf = require("../conf"),
	FAQModel = mongoose.model("faq");

FAQ = {

	findOne : function (filter, callback) {
		FAQModel.findOne(filter, callback);
	}
};

module.exports = FAQ;