module.exports = {
	viewspath : __dirname + "/views/",
	modulepath : "/",
	title : "FAQ",
	getMenu : function () {
		var menu = [
			{
				path : "/",
				label : "Homepage"
			},
			{
				path : this.modulepath,
				label : "Ajouts récents"
			},
			{
				path : this.modulepath + "/search",
				label : "Rechercher"
			},
			{
				path : this.modulepath + "/create",
				label : "Nouvelle question"
			}
		];
		return menu;
	},
	getHeader : function () {
		return {
			menu : this.getMenu(),
			title : this.title,
			modulepath : this.modulepath
		}
	}
};